﻿using System;
using System.IO;
using System.Text;

namespace Libraries.JSON
{
    public class JSONException : Exception
    {
        public JSONException(string msg) : base(msg)
        { }
    }

    public static class JSONReader
    {
        public class InvalidJSONContentException : JSONException
        {
            public InvalidJSONContentException(string content) : base("Unable to parse: " + content)
            { }

            public InvalidJSONContentException(string content, string reason) : base("Unable to parse: " + content + "   (" + reason + ")")
            { }
        }

        public static JSONDataSetToRead ReadFile(string fileName)
        {
            return Read(File.ReadAllText(fileName));
        }

        public static JSONDataSetToRead Read(string JSONContent)
        {
            JSONContent = СlearDataString(JSONContent);
            bool arrayMode;

            if (JSONContent[0] == '{' && JSONContent[JSONContent.Length - 1] == '}')
                arrayMode = false;
            else if (JSONContent[0] == '[' && JSONContent[JSONContent.Length - 1] == ']')
                arrayMode = true;
            else throw new InvalidJSONContentException(JSONContent, "bracket count mismatch");
            JSONContent = JSONContent.Substring(1, JSONContent.Length - 2); //rm first and last char (brackets)

            JSONDataSetToRead result = new JSONDataSetToRead();

            int cursor = 0;

            string name, content;

            while (cursor < JSONContent.Length)
            {
                if (!arrayMode)
                {
                    name = GetContentName(ref cursor, JSONContent);

                    if (cursor == JSONContent.Length || JSONContent[cursor] != ':')
                        throw new InvalidJSONContentException(JSONContent);
                    cursor++;
                }
                else name = null;

                if (cursor == JSONContent.Length || JSONContent[cursor] == ',' ||           //detect empty value
                        cursor + 1 < JSONContent.Length &&
                        (JSONContent[cursor] != '{' && JSONContent[cursor + 1] == '}' ||   //detect empty object
                        JSONContent[cursor] != '[' && JSONContent[cursor + 1] == ']'))     //detect empty array
                    throw new InvalidJSONContentException(JSONContent, "empty parameter");

                if (JSONContent[cursor] == '{')
                    content = GetBracketedContent(ref cursor, JSONContent, "{}");
                else if (JSONContent[cursor] == '[')
                    content = GetBracketedContent(ref cursor, JSONContent, "[]");
                else if (JSONContent[cursor] == '"')
                    content = GetQuotedContent(ref cursor, JSONContent);
                else
                    content = GetSimpleContent(ref cursor, JSONContent);

                if (arrayMode)
                    result.Add(new JSONDataToRead(null, content));
                else
                    result.Add(new JSONDataToRead(name, content));
            }

            return result;
        }

        static string GetContentName(ref int cursor, string toParse)
        {
            int tmpCursor;

            if (toParse[cursor] != '"') throw new InvalidJSONContentException(toParse);
            cursor++;

            for (tmpCursor = cursor; tmpCursor < toParse.Length && toParse[tmpCursor] != '"'; tmpCursor++) ;
            if (tmpCursor == toParse.Length) throw new InvalidJSONContentException(toParse);
            string name = toParse.Substring(cursor, tmpCursor - cursor);
            cursor = tmpCursor + 1;

            return name;
        }

        static string GetQuotedContent(ref int cursor, string toParse)
        {
            string result = GetContentName(ref cursor, toParse);

            if (cursor < toParse.Length && toParse[cursor] != ',') throw new InvalidJSONContentException(toParse);
            cursor++;

            return result;
        }

        static string GetSimpleContent(ref int cursor, string toParse)
        {
            int tmpCursor;

            for (tmpCursor = cursor; tmpCursor < toParse.Length && toParse[tmpCursor] != ','; tmpCursor++) ;

            string content = toParse.Substring(cursor, tmpCursor - cursor);
            cursor = tmpCursor + 1;

            return content;
        }

        static string GetBracketedContent(ref int cursor, string toParse, string bracketPair)
        {
            if (bracketPair.Length != 2)
                throw new Exception();

            int tmpCursor;
            int brackets = 0;
            for (tmpCursor = cursor; tmpCursor < toParse.Length; tmpCursor++)
            {
                if (toParse[tmpCursor] == '"')
                    for (tmpCursor++; tmpCursor < toParse.Length && toParse[tmpCursor] != '"'; tmpCursor++) ;

                if (toParse[tmpCursor] == bracketPair[0])
                    brackets++;
                else if (toParse[tmpCursor] == bracketPair[1])
                    brackets--;

                if (brackets == 0)
                    break;
            }
            if (tmpCursor == toParse.Length) throw new InvalidJSONContentException(toParse, "bracket count mismatch");
            string content = toParse.Substring(cursor, tmpCursor - cursor + 1); //"{" + content + "}"
            cursor = tmpCursor + 1;
            if (cursor < toParse.Length && toParse[cursor] != ',') throw new InvalidJSONContentException(toParse);
            cursor++;

            return content;
        }

        static string СlearDataString(string s)
        {
            StringBuilder sb = new StringBuilder();
            bool insideQuotes = false;
            bool insideSingleLineComment = false;
            bool insideMultiLineComment = false;
            for (int i = 0; i < s.Length; i++)
            {
                /*comments*/
                if (i + 1 < s.Length && s[i] == '/' && s[i + 1] == '*')
                    insideMultiLineComment = true;
                else if (i > 2 && s[i - 2] == '*' && s[i - 1] == '/')
                    insideMultiLineComment = false;

                if (i + 1 < s.Length && s[i] == '/' && s[i + 1] == '/')
                    insideSingleLineComment = true;
                else if (i > 0 && s[i - 1] == '\n')
                    insideSingleLineComment = false;

                if (insideMultiLineComment || insideSingleLineComment)
                    continue;
                /*comments*/

                /*quotes*/
                if (s[i] == '"')
                    insideQuotes = !insideQuotes;

                if (insideQuotes)
                    sb.Append(s[i]);
                else if (!char.IsWhiteSpace(s[i]))
                    sb.Append(s[i]);
                /*quotes*/
            }
            return sb.ToString();
        }
    }
}
