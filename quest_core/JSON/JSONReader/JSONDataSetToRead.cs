﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libraries.JSON
{
    public class JSONDataSetToRead : List<JSONDataToRead>
    {
        public JSONDataToRead this[string name]
        {
            get => Find(data => data.name == name);
        }

        public bool TryGetData(string name, out JSONDataToRead data)
        {
            int index = FindIndex(d => d.name == name);
            if (index != -1)
            {
                data = this[index];
                return true;
            }
            data = default;
            return false;
        }
    }
}
