﻿using Libraries.JSON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace quest_core
{
    class Program
    {
        static void Main(string[] args)
        {
            var quests = JSONReader.ReadFile("demo_quest.json");
            var quest = new Quest(quests["Quests"][0]);
            while (quest != null)
            {
                Console.WriteLine("Title: " + quest.Title);
                Console.WriteLine("Duration: " + quest.Duration + "min\n");

                while (!quest.Finished)
                {
                    var currBlock = quest.GetCurrentBlock();
                    Console.WriteLine(currBlock.Phraze);
                    for (int i = 0; i < currBlock.Answers.Length; i++)
                        Console.WriteLine(i + " - " + currBlock.Answers[i]);

                    Console.WriteLine();
                    quest.Answer(int.Parse(Console.ReadLine()));
                    Console.WriteLine();
                }

                Console.WriteLine("Поздравляю! Ты завершил этот квест и получил " + quest.Exp + " Exp");
                if (quest.NextQuest.Length > 0)
                {
                    foreach (JSONDataToRead iter in quests["Quests"])
                        if (iter["Title"].GetValue<string>() == quest.NextQuest)
                            quest = new Quest(iter);

                    Console.WriteLine("Начинаем новый квест\n\n");
                }
                else
                    quest = null;
            }

            Console.WriteLine("Ура! ты завершил все квесты!");
            Console.ReadLine();
        }
    }

    public class Quest
    {
        public class QuestBlock
        {
            private JSONDataToRead content;
            private Quest handlerQuest;

            public QuestBlock(JSONDataToRead content, Quest handlerQuest)
            {
                this.content = content;
                this.handlerQuest = handlerQuest;
            }

            public string Phraze { get { return content["Text"].GetValue<string>(); } }
            public string[] Answers { get { return handlerQuest.getAnswers(content).Select(answer => answer["Text"].GetValue<string>()).ToArray(); } }
        }

        private JSONDataToRead content;
        private int currentId;

        public int Duration
        {
            get
            {
                string duration = content["Duration"].GetValue<string>();
                string[] separated = duration.Split('d', 'h', 'm').Where(str => str.Length > 0).ToArray();
                string[] dhm = duration.Split('1', '2', '3', '4', '5', '6', '7', '8', '9').Where(str => str.Length > 0).ToArray();
                int result = 0;

                for (int i = 0; i < dhm.Length; i++)
                {
                    switch (dhm[i])
                    {
                        case "m":
                            result += int.Parse(separated[i]);
                            break;

                        case "h":
                            result += int.Parse(separated[i]) * 60;
                            break;

                        case "d":
                            result += int.Parse(separated[i]) * 60 * 24;
                            break;
                    }
                }
                return result;
            }
        }
        public int Exp { get { return content["Exp"].GetValue<int>(); } }
        public string Title { get { return content["Title"].GetValue<string>(); } }
        public string NextQuest
        {
            get
            {
                if (content.TryGetData("NextQuest", out string nextQuest))
                    return nextQuest;
                else
                    return "";
            }
        }
        public bool Finished { get { return currentId < 0; } }

        public Quest(JSONDataToRead content)
        {
            this.content = content;
            currentId = 0;
        }

        public QuestBlock GetCurrentBlock()
        {
            return new QuestBlock(findBlockByID(currentId), this);
        }

        public void Answer(int answerNum)
        {
            currentId = getAnswers(findBlockByID(currentId))[answerNum]["NextID"].GetValue<int>();
        }

        private JSONDataToRead findBlockByID(int id)
        {
            foreach (JSONDataToRead iterBlock in content["Content"])
                if (iterBlock["ID"].GetValue<int>() == id)
                    return iterBlock;
            return null;
        }

        private JSONDataSetToRead getAnswers(JSONDataToRead block)
        {
            if (block.TryGetData("Answers", out JSONDataSetToRead answers))
                return answers;

            if (block.TryGetData("AnswersID", out int answersID))
                return getAnswers(findBlockByID(answersID));

            return null;
        }
    }
}
