﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tutorial.SqlConn;
using System.Data.SqlClient;
using System.Data.Common;

namespace ConnectSQLServer
{
    class Progress
    {
        
        public static void AddQuest(SqlConnection conn, string userLogin, string questName) // РАБОТАЕТ
        {
            conn.Open();
            /*
                добавление нового квеста в список квестов пользователя
                по умолчанию прогресс устанавливается на 0 очков, время окончания null
            */
            SqlCommand cmd = new SqlCommand();

            string sql = "insert into UsersProgress values ('" + questName + "', '" + userLogin + "', getdate(), default, default)";

            cmd.Connection = conn;
            cmd.CommandText = sql;
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public static void UpdateQuestProgress(SqlConnection conn, string userLogin, string questName, int newScore) // РАБОТАЕТ
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand();

            string sql = "update UsersProgress set QuestScore = " + newScore + " where QuestTitle = '" + questName + "' and UserName = '" + userLogin + "'";
            cmd.Connection = conn;
            cmd.CommandText = sql;
            int a = cmd.ExecuteNonQuery();
            conn.Close();
        }

        public static void FinishQuest(SqlConnection conn, string userLogin, string questName) // РАБОТАЕТ
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand();

            string sql = "update UsersProgress set QuestFinish = getdate() where QuestTitle = '" + questName + "' and UserName = '" + userLogin + "'";

            cmd.Connection = conn;
            cmd.CommandText = sql;
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public static int GetQuestProgress(SqlConnection conn, string userLogin, string questName) // РАБОТАЕТ
        {
            SqlCommand cmd = new SqlCommand();
            conn.Open();

            string sql = "select * from UsersProgress where QuestTitle = '" + questName + "' and UserName = '" + userLogin + "'";

            cmd.Connection = conn;
            cmd.CommandText = sql;
            //cmd.ExecuteNonQuery();
            int res = 0;

            using (DbDataReader reader = cmd.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int column = reader.GetOrdinal("QuestScore");
                        res = Convert.ToInt32(reader.GetValue(column));
                    }
                }
            }
            conn.Close();
            return res;
        }

        public static string GetQuestFinish(SqlConnection conn, string userLogin, string questName) // РАБОТАЕТ
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand();

            string sql = "select * from UsersProgress where QuestTitle = '" + questName + "' and UserName = '" + userLogin + "'";

            cmd.Connection = conn;
            cmd.CommandText = sql;

            string res = "";

            using (DbDataReader reader = cmd.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int column = reader.GetOrdinal("QuestFinish");
                        res = Convert.ToString(reader.GetValue(column));
                    }
                }
            }
            conn.Close();
            return res;
        }

    }
}
