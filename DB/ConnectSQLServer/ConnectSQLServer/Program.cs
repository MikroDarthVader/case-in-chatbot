﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tutorial.SqlConn;
using System.Data.SqlClient;
using System.Data.Common;
using System.Net.Sockets;
using System.Net;

namespace ConnectSQLServer
{



    class Program
    {

        private static void QueryUsers(SqlConnection conn)
        {
            string str = "Users";
            string sql = "Select ID from " + str;
            //string sql = "create table users123 (ID int, names varchar(255));";
            // Создать объект Command.
            SqlCommand cmd = new SqlCommand();

            // Сочетать Command с Connection.
            cmd.Connection = conn;
            cmd.CommandText = sql;


            using (DbDataReader reader = cmd.ExecuteReader())
            {
                //Console.WriteLine("So far so good");
                if (reader.HasRows)
                {
                    //Console.WriteLine("Still going");
                    while (reader.Read())
                    {
                        //Console.WriteLine("Iteration..");
                        int userID = reader.GetOrdinal("ID"); // 0
                        long empId = Convert.ToInt64(reader.GetValue(userID));
                        //int str = Convert.ToInt32(reader.GetString(userID));
                        Console.WriteLine(empId);
                        Console.WriteLine("=====================");
                        // Индекс столбца ID в команде SQL.



                        //long empId = Convert.ToInt64(reader.GetValue(0));

                        // Столбец Emp_No имеет index = 1.
                        /*string empNo = reader.GetString(1);
                        int regdate = reader.GetOrdinal("RegTime");// 2
                        string registerdate = reader.GetString(regdate);

                        // Индекс столбца Mng_Id в команде SQL.

                        long? mngId = null;


                        if (!reader.IsDBNull(userID))
                        {
                            mngId = Convert.ToInt64(reader.GetValue(userID));
                        }
                        Console.WriteLine("--------------------");
                        Console.WriteLine("userID:" + userID);
                        Console.WriteLine("RegDate:" + registerdate);*/

                    }
                }
            }

        }

        static void Main(string[] args)
        {
            string str = Console.ReadLine();
            if (str == "0")
            {
                Console.WriteLine("Server");
                const int port = 8888; // порт для прослушивания подключений
                TcpListener server = null;
                try
                {
                    IPAddress localAddr = IPAddress.Parse("127.0.0.1");
                    server = new TcpListener(localAddr, port);

                    // запуск слушателя
                    server.Start();

                    while (true)
                    {
                        Console.WriteLine("Ожидание подключений... ");

                        // получаем входящее подключение
                        TcpClient client = server.AcceptTcpClient();
                        Console.WriteLine("Подключен клиент. Выполнение запроса...");

                        // получаем сетевой поток для чтения и записи
                        NetworkStream stream = client.GetStream();

                        // сообщение для отправки клиенту
                        string response = "Привет мир";
                        // преобразуем сообщение в массив байтов
                        byte[] data = Encoding.UTF8.GetBytes(response);

                        // отправка сообщения
                        stream.Write(data, 0, data.Length);
                        Console.WriteLine("Отправлено сообщение: {0}", response);
                        // закрываем поток
                        stream.Close();
                        // закрываем подключение
                        client.Close();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    if (server != null)
                        server.Stop();
                }
            }
            else
            {
                Console.WriteLine("Client");
                int port = 8888;
                string server = "127.0.0.1";

                try
                {
                    TcpClient client = new TcpClient();
                    client.Connect(server, port);

                    byte[] data = new byte[256];
                    StringBuilder response = new StringBuilder();
                    NetworkStream stream = client.GetStream();

                    do
                    {
                        int bytes = stream.Read(data, 0, data.Length);
                        response.Append(Encoding.UTF8.GetString(data, 0, bytes));
                    }
                    while (stream.DataAvailable); // пока данные есть в потоке

                    Console.WriteLine(response.ToString());

                    // Закрываем потоки
                    stream.Close();
                    client.Close();
                }
                catch (SocketException exc)
                {
                    Console.WriteLine("SocketException: {0}", exc);
                }
                catch (Exception exc)
                {
                    Console.WriteLine("Exception: {0}", exc.Message);
                }
            }

            //Console.WriteLine("Getting Connection ...");

            //string datasource = @"(LocalDB)\MSSQLLocalDB";

            //string database = "CbotDbase";
            //string username = "Eclipse3";
            //string password = "nqq6n5ns";

            //string connString = @"Data Source=" + datasource +
            //            /*";Initial Catalog=" + database + */
            //            ";AttachDbFilename = D:\\Projects\\case-in-chatbot\\DB\\ConnectSQLServer\\CbotDbase.mdf" +
            //            ";Database = CbotDbase" +
            //            ";User ID = " + username +
            //            ";Password = " + password;

            ////Console.WriteLine(connString);


            //SqlConnection conn = DBUtils.GetDBConnection();

            ////Slass.CreateProgressTable(conn);
            ////Slass.AddQuest(conn, "User123", "FirstQuest");
            ////Slass.UpdateQuestProgress(conn, "User12345", "FirstQuest", 6);
            ////Slass.FinishQuest(conn, "User123", "ThirdQuest");
            ////Slass.AddQuest(conn, "User12345", "FirstQuest");

            //bool a = Users.AddUser(conn, "Eclipse", "qwerty", "Misha", "file", "123123");
            //if (a)
            //{
            //    Console.WriteLine("Register successful");
            //}
            //else Console.WriteLine("Register unsuccessful");

            //a = Users.Login(conn, "Eclipse", "qwerty");
            //if (a)
            //{
            //    Console.WriteLine("Login successful");
            //}
            //else Console.WriteLine("Login unsuccessful");
            //string file = Users.GetFile(conn, "Eclipse");
            //Console.WriteLine("File name = " + file);


            //try
            //{
            //    Console.WriteLine("Openning Connection ...");

            //    conn.Open();

            //    Console.WriteLine("Connection successful!");
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine("Error: " + e.Message);
            //}
            ////////////////////////////////////////////////////////////
            //Console.Read();

            //    // Получить объект Connection подключенный к DB.
            //    SqlConnection conne = DBUtils.GetDBConnection();
            //    conne.Open();
            //    try
            //    {
            //        QueryUsers(conne);
            //    }
            //    catch (Exception e)
            //    {
            //        Console.WriteLine("Error: " + e);
            //        Console.WriteLine(e.StackTrace);
            //    }
            //    finally
            //    {
            //        // Закрыть соединение.
            //        conne.Close();
            //        // Разрушить объект, освободить ресурс.
            //        conne.Dispose();
            //    }
            //    Console.Read();
            //    Console.Read();
            //    Console.Read();



        }

    }
}
