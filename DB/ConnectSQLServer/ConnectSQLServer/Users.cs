﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tutorial.SqlConn;
using System.Data.SqlClient;
using System.Data.Common;

namespace ConnectSQLServer
{
    class Users
    {
        public static bool Login(SqlConnection conn, string userLogin, string userPassword) // РАБОТАЕТ
        {
            SqlCommand cmd = new SqlCommand();
            conn.Open();

            string sql = "select count(*) as col from UserList where UserLogin = '" + userLogin + "' and UserPassword = '" + userPassword + "'";

            cmd.Connection = conn;
            cmd.CommandText = sql;
            int res = 0;

            using (DbDataReader reader = cmd.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int column = reader.GetOrdinal("col");
                        res = Convert.ToInt32(reader.GetValue(column));
                    }
                }
            }
            conn.Close();
            if (res == 1) { return true; }

            return false;
        }

        public static bool AddUser(SqlConnection conn, string userLogin, string userPassword, string userName, string fileName, string mac) // РАБОТАЕТ
        {
            conn.Open();

            SqlCommand cmd = new SqlCommand();

            string sql = "select count(*) as col from UserList where UserLogin = '" + userLogin + "'";

            cmd.Connection = conn;
            cmd.CommandText = sql;

            int count = 0;
            using (DbDataReader reader = cmd.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int column = reader.GetOrdinal("col");
                        count = Convert.ToInt32(reader.GetValue(column));
                    }
                }
            }

            if (count == 1)
            {
                conn.Close();
                return false;
            }
            else
            {
                sql = "insert into UserList values ('" + userLogin + "', '" + userPassword + "', '" + userName + "', '" + fileName + "', '" + mac + "', getdate())";


                cmd.ExecuteNonQuery();
                conn.Close();
                return true;
            }

        }

        public static string GetName(SqlConnection conn, string userLogin) // РАБОТАЕТ
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand();

            string sql = "select * from UserList where UserLogin = '" + userLogin + "'";

            cmd.Connection = conn;
            cmd.CommandText = sql;

            string res = "";

            using (DbDataReader reader = cmd.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int column = reader.GetOrdinal("UserName");
                        res = Convert.ToString(reader.GetValue(column));
                    }
                }
            }
            conn.Close();
            return res;
        }

        public static string GetFile(SqlConnection conn, string userLogin) // РАБОТАЕТ
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand();

            string sql = "select * from UserList where UserLogin = '" + userLogin + "'";

            cmd.Connection = conn;
            cmd.CommandText = sql;

            string res = "";

            using (DbDataReader reader = cmd.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int column = reader.GetOrdinal("UserFileName");
                        res = Convert.ToString(reader.GetValue(column));
                    }
                }
            }
            conn.Close();
            return res;
        }

        public static string GetMac(SqlConnection conn, string userLogin) // РАБОТАЕТ
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand();

            string sql = "select * from UserList where UserLogin = '" + userLogin + "'";

            cmd.Connection = conn;
            cmd.CommandText = sql;

            string res = "";

            using (DbDataReader reader = cmd.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int column = reader.GetOrdinal("UserMac");
                        res = Convert.ToString(reader.GetValue(column));
                    }
                }
            }
            conn.Close();
            return res;
        }
    }
}
