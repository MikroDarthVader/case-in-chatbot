﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Tutorial.SqlConn
{
    class DBUtils
    {
        public static SqlConnection GetDBConnection()
        {
            string datasource = @"(LocalDB)\MSSQLLocalDB";

            string database = " ";
            string username = " ";
            string password = " ";

            string progressTable = "UsersProgress";

            return DBSQLServerUtils.GetDBConnection(datasource, database, username, password);
        }
    }

}