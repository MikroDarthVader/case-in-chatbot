﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace ClientApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ShellFlyout : ContentPage
    {
        public ListView ListView;

        public ShellFlyout()
        {
            InitializeComponent();

            BindingContext = new ShellFlyoutViewModel();
            ListView = MenuItemsListView;
        }

        class ShellFlyoutViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<ShellFlyoutMenuItem> MenuItems { get; set; }

            public ShellFlyoutViewModel()
            {
                MenuItems = new ObservableCollection<ShellFlyoutMenuItem>(new[]
                {
                    new ShellFlyoutMenuItem { Id = 0, Title = "Квесты", TargetType = typeof(CurrentChats)},
                    new ShellFlyoutMenuItem { Id = 1, Title = "Архив", TargetType = typeof(ArchivedChats)},
                    new ShellFlyoutMenuItem { Id = 2, Title = "Таблица лидеров", TargetType = typeof(LeaderBoard)},
                });
            }

            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}