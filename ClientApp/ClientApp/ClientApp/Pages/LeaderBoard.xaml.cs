﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClientApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LeaderBoard : ContentPage
    {
        public LeaderBoard()
        {
            InitializeComponent();

            BindingContext = new LeaderBoardViewModel();
        }

        class LeaderBoardViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<LeaderBoardItem> LeaderBoardItems { get; set; }

            public LeaderBoardViewModel()
            {
                LeaderBoardItems = new ObservableCollection<LeaderBoardItem>(new[]
                {
                    new LeaderBoardItem { Id = 0, Name = "Евгений Пушной", Score = 120},
                    new LeaderBoardItem { Id = 1, Name = "Борис Кузнецов", Score = 110},
                    new LeaderBoardItem { Id = 2, Name = "Татьяна Рыбакова", Score = 110},
                    new LeaderBoardItem { Id = 3, Name = "Константин Вараксов", Score = 90},
                    new LeaderBoardItem { Id = 4, Name = "Ярослав Серов", Score = 80},
                    new LeaderBoardItem { Id = 11, Name = "Ты", Score = 40},
                });
            }

            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}