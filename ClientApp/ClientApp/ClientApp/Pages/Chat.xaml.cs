﻿using ClientApp.Systems;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClientApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Chat : ContentPage
    {

        public Dictionary<Button, int> buttonsInd;

        public Chat()
        {
            InitializeComponent();

            buttonsInd = new Dictionary<Button, int>();
            ChatPage.Disappearing += ChatPage_Disappearing;
            ChatListView.BindingContextChanged += ChatListView_BindingContextChanged;
        }

        private void ChatListView_BindingContextChanged(object sender, EventArgs e)
        {
            ChatListView.ScrollTo(((ChatViewModel)BindingContext).ChatItems.Last(), ScrollToPosition.End, false);
        }

        private void ChatPage_Disappearing(object sender, EventArgs e)
        {
            ((ChatViewModel)BindingContext).quest.Reset();
        }

        private Button GetButton(string text, int ind)
        {
            var button = new Button
            {
                Text = text,
                BackgroundColor = Color.FromHex("4B4B4B"),
                TextColor = Color.FromHex("FFFFFF"),
                TextTransform = TextTransform.None,
                FontSize = 16,
                Padding = new Thickness(10, 5),
                Margin = new Thickness(20, 0, 20, 0)
            };

            buttonsInd.Add(button, ind);
            button.Clicked += Button_Clicked;
            return button;
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            ((ChatViewModel)BindingContext).Update(buttonsInd[(Button)sender]);
            UpdateButtons();
        }

        public void UpdateButtons()
        {
            buttonsInd.Clear();
            ButtonsStackLayout.Children.Clear();

            var currBlock = ((ChatViewModel)BindingContext).quest.GetCurrentBlock();
            for (int i = 0; i < currBlock.Answers.Length; i++)
                ButtonsStackLayout.Children.Add(GetButton(currBlock.Answers[i], i));
        }

        public class ChatViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<ChatItem> ChatItems { get; set; }
            public Quest quest { get; set; }
            public QuestState questState { get; set; }

            public ChatViewModel(Quest quest, QuestState questState)
            {
                this.quest = quest;
                this.questState = questState;

                ChatItems = new ObservableCollection<ChatItem>();

                Quest.QuestBlock currBlock = quest.GetCurrentBlock();

                ChatItems.Add(new ChatItem { id = 0, Message = currBlock.Phraze, IsBotSender = true });

                foreach (var button in questState.buttonHistory)
                {
                    ChatItems.Add(new ChatItem { id = 1, Message = currBlock.Answers[button], IsBotSender = false });
                    quest.Answer(button);

                    currBlock = quest.GetCurrentBlock();
                    ChatItems.Add(new ChatItem { id = 1, Message = currBlock.Phraze, IsBotSender = true });
                }
            }

            public void Update(int buttonIndex)
            {
                var currBlock = quest.GetCurrentBlock();
                ChatItems.Add(new ChatItem { id = 1, Message = currBlock.Answers[buttonIndex], IsBotSender = false });
                quest.Answer(buttonIndex);
                questState.buttonHistory.AddLast(buttonIndex);
                currBlock = quest.GetCurrentBlock();
                ChatItems.Add(new ChatItem { id = 1, Message = currBlock.Phraze, IsBotSender = true });

                var questsState = GlobalSpace.questsStateManager.QuestsStates;
                var quests = GlobalSpace.quests;

                if (quest.Finished)
                {
                    foreach(var q in quests)
                        if (q.Title == quest.NextQuest)
                        {
                            var questState = new QuestState { startTime = DateTime.Now, buttonHistory = new LinkedList<int>() };
                            questsState.Add(q.Title, questState);
                        }
                }

                GlobalSpace.questsStateManager.SyncWithFile();
            }

            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}
