﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using ClientApp.Systems;

namespace ClientApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CurrentChats : ContentPage
    {
        public CurrentChats()
        {
            InitializeComponent();

            BindingContext = new CurrentChatsViewModel();
            CurrentChatsListView.ItemSelected += ListView_ItemSelected;
            CurrentChatsPage.Appearing += CurrentChatsPage_Appearing;
        }

        private void CurrentChatsPage_Appearing(object sender, EventArgs e)
        {
            var items = ((CurrentChatsViewModel)BindingContext).CurrentChatsItems;

            var questsState = GlobalSpace.questsStateManager.QuestsStates;
            var quests = GlobalSpace.quests;

            int id = 0;
            foreach (var questState in questsState)
            {
                var quest = quests.Find(q => q.Title == questState.Key);

                bool exists = false;
                for (int i = 0; i < items.Count; i++)
                {
                    if (items[i].Title == questState.Key)
                        exists = true;
                }

                if (questState.Value.buttonHistory.Count == 0 && !exists)
                    items.Add(new CurrentChatsItem { Id = id, Quest = quest, State = questState.Value });

                id++;
            }

            for (int i = 0; i < items.Count; i++)
            {
                foreach (var button in items[i].State.buttonHistory)
                    items[i].Quest.Answer(button);
                if (items[i].Quest.Finished)
                {
                    items.RemoveAt(i);
                    i--;
                }
                else
                    items[i].Quest.Reset();
            }
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as CurrentChatsItem;
            if (item == null)
                return;

            item.Quest.Reset();
            var page = new Chat { BindingContext = new Chat.ChatViewModel(item.Quest, item.State) };
            page.UpdateButtons();
            Navigation.PushAsync(page);
            page.Title = item.Title;

            CurrentChatsListView.SelectedItem = null;
        }

        class CurrentChatsViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<CurrentChatsItem> CurrentChatsItems { get; set; }

            public CurrentChatsViewModel()
            {
                CurrentChatsItems = new ObservableCollection<CurrentChatsItem>();



            }

            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}