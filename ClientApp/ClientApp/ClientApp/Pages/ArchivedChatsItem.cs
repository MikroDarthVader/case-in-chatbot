﻿using ClientApp.Systems;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClientApp
{
    public class ArchivedChatsItem
    {
        public int Id { get; set; }
        public Quest Quest { get; set; }
        public QuestState State { get; set; }
        public string Title { get { return Quest.Title; } }
        public string LastMessage
        {
            get
            {
                Quest.Reset();
                foreach (var button in State.buttonHistory)
                    Quest.Answer(button);

                string result = Quest.GetCurrentBlock().Phraze;
                Quest.Reset();
                return result;
            }
        }
    }
}
