﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientApp
{
    public class LeaderBoardItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Score { get; set; }
        public int FormattedId { get { return Id + 1; } }
        public string FormattedScore { get { return Score + " exp"; } }
    }
}
