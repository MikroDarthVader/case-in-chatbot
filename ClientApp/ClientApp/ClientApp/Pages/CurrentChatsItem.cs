﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

using ClientApp.Systems;

namespace ClientApp
{
    public class CurrentChatsItem
    {
        public int Id { get; set; }
        public Quest Quest { get; set; }
        public QuestState State { get; set; }
        public string Title { get { return Quest.Title; } }
        public string LastMessage
        {
            get
            {
                Quest.Reset();
                foreach (var button in State.buttonHistory)
                        Quest.Answer(button);

                string result = Quest.GetCurrentBlock().Phraze;
                Quest.Reset();
                return result;
            }
        }
        public string TimeLeft
        {
            get
            {
                var diff = DateTime.Now - State.startTime;
                var minutes = Quest.Duration - diff.Days * 24 * 60 - diff.Hours * 60 - diff.Minutes;

                int day = minutes / (24 * 60);
                int hour = (minutes - day * (24 * 60)) / 60;
                int min = (minutes - day * (24 * 60) - hour * 60);
                return day + " д " + hour + " ч " + min + " м";
            }
        }
        public Color StatusColor { get { if (State.buttonHistory.Count != 0) return Color.FromHex("4784C0"); else return Color.FromHex("AF3232"); } }
    }
}
