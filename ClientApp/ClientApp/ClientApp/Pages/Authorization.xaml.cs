﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using ClientApp.Systems;

namespace ClientApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Authorization : ContentPage
    {
        public Authorization()
        {
            InitializeComponent();

            EnterButton.Clicked += EnterButton_Clicked;
        }

        private void EnterButton_Clicked(object sender, EventArgs e)
        {
            GlobalSpace.questsStateManager.DeleteFile();

            //int port = 8888;
            //string server = "127.0.0.1";

            //try
            //{
            //    TcpClient client = new TcpClient();
            //    client.Connect(server, port);

            //    byte[] data = new byte[256];
            //    StringBuilder response = new StringBuilder();
            //    NetworkStream stream = client.GetStream();

            //    do
            //    {
            //        int bytes = stream.Read(data, 0, data.Length);
            //        response.Append(Encoding.UTF8.GetString(data, 0, bytes));
            //    }
            //    while (stream.DataAvailable); // пока данные есть в потоке

            //    Console.WriteLine(response.ToString());

            //    // Закрываем потоки
            //    stream.Close();
            //    client.Close();
            //}
            //catch (SocketException exc)
            //{
            //    Console.WriteLine("SocketException: {0}", exc);
            //}
            //catch (Exception exc)
            //{
            //    Console.WriteLine("Exception: {0}", exc.Message);
            //}

            var questsState = GlobalSpace.questsStateManager.QuestsStates;
            var quests = GlobalSpace.quests;

            foreach (var quest in quests)
            {
                if (!questsState.ContainsKey(quest.Title))
                {
                    bool havePrev = false;
                    foreach (var q in quests)
                        if (q.NextQuest == quest.Title)
                            havePrev = true;

                    if (!havePrev)
                    {
                        var questState = new QuestState { startTime = DateTime.Now, buttonHistory = new LinkedList<int>() };
                        questsState.Add(quest.Title, questState);
                    }
                }
            }

            GlobalSpace.questsStateManager.SyncWithFile();
            Navigation.PushModalAsync(new Shell());
        }
    }
}