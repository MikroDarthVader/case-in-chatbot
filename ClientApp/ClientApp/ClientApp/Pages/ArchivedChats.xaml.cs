﻿using ClientApp.Systems;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClientApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ArchivedChats : ContentPage
    {
        public ArchivedChats()
        {
            InitializeComponent();

            BindingContext = new ArchivedChatsViewModel();
            ArchivedChatsListView.ItemSelected += ListView_ItemSelected;
            ArchivedChatsPage.Appearing += ArchivedChatsPage_Appearing;
        }

        private void ArchivedChatsPage_Appearing(object sender, EventArgs e)
        {
            var items = ((ArchivedChatsViewModel)BindingContext).ArchivedChatsItems;

            var questsState = GlobalSpace.questsStateManager.QuestsStates;
            var quests = GlobalSpace.quests;

            int id = 0;
            foreach (var questState in questsState)
            {
                var quest = quests.Find(q => q.Title == questState.Key);

                bool exists = false;
                for (int i = 0; i < items.Count; i++)
                {
                    if (items[i].Title == questState.Key)
                        exists = true;
                }

                quest.Reset();
                foreach (var button in questState.Value.buttonHistory)
                    quest.Answer(button);

                if (quest.Finished && !exists)
                    items.Add(new ArchivedChatsItem { Id = id, Quest = quest, State = questState.Value });

                quest.Reset();

                id++;
            }
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as ArchivedChatsItem;
            if (item == null)
                return;

            item.Quest.Reset();
            var page = new Chat { BindingContext = new Chat.ChatViewModel(item.Quest, item.State) };
            Navigation.PushAsync(page);
            page.Title = item.Title;

            ArchivedChatsListView.SelectedItem = null;
        }

        class ArchivedChatsViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<ArchivedChatsItem> ArchivedChatsItems { get; set; }

            public ArchivedChatsViewModel()
            {
                ArchivedChatsItems = new ObservableCollection<ArchivedChatsItem>();
            }

            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}