﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ClientApp
{
    public class ChatItem
    {
        public int id { get; set; }
        public string Message { get; set; }
        public bool IsBotSender { get; set; }
        public Thickness MessageMargin { get { if (IsBotSender) return new Thickness(6, id == 0 ? 6 : 0, 50, 6); else return new Thickness(50, id == 0 ? 6 : 0, 6, 6); } }
        public LayoutOptions MessageHorOpt { get { if (IsBotSender) return LayoutOptions.Start; else return LayoutOptions.End; } }
        public Color MessageColor { get { if (IsBotSender) return Color.FromHex("282828"); else return Color.FromHex("4784C0"); } }
    }
}
