﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Libraries.JSON;

namespace ClientApp.Systems
{
    public class QuestsStateManager
    {
        private static readonly string QuestsStateFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "QuestsState.json");


        public Dictionary<string, QuestState> QuestsStates;

        public QuestsStateManager()
        {
            QuestsStates = new Dictionary<string, QuestState>();
        }

        public void ReadQuestsState()
        {
            if (File.Exists(QuestsStateFilePath))
            {
                var content = JSONReader.ReadFile(QuestsStateFilePath);

                foreach (var quest in content)
                {
                    var questState = new QuestState();
                    questState.startTime = DateTime.Parse(quest["StartTime"].GetValue<string>());
                    foreach (JSONDataToRead id in quest["ButtonHistory"])
                        questState.buttonHistory.AddLast(id.GetValue<int>());

                    QuestsStates.Add(quest["Title"].GetValue<string>(), questState);
                }
            }
        }

        public void SyncWithFile()
        {
            var data = JSONWriter.GetDataStoreRoot(true);
            foreach (var questState in QuestsStates)
            {
                var state = data.AddDataSet();
                state.AddData("Title", questState.Key);
                state.AddData("StartTime", questState.Value.startTime.ToString());
                var history = state.AddArray("ButtonHistory");
                foreach (var id in questState.Value.buttonHistory)
                    history.AddData(id);
            }

            JSONWriter.WriteToFile(QuestsStateFilePath, data);
        }

        public void DeleteFile()
        {
            if (File.Exists(QuestsStateFilePath))
                File.Delete(QuestsStateFilePath);
        }
    }

    public class QuestState
    {
        public DateTime startTime;
        public LinkedList<int> buttonHistory;

        public QuestState()
        {
            buttonHistory = new LinkedList<int>();
        }
    }
}
